import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {MatInputModule} from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
import {EditorModule} from 'primeng/editor';

@NgModule({
  imports: [
    CommonModule,InputTextModule,DropdownModule,MatInputModule,
    MatDatepickerModule,MatFormFieldModule,MatNativeDateModule,
    BrowserAnimationsModule,MatSelectModule,EditorModule
  ],
  declarations: [],
  exports:[InputTextModule,DropdownModule,MatInputModule,
    MatDatepickerModule,MatFormFieldModule,MatNativeDateModule,
    BrowserAnimationsModule,MatSelectModule,EditorModule]
})
export class SharedModule { }
