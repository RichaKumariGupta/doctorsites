import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '../../../node_modules/@angular/router';
import { SharedModule } from '../shared/shared.module';
import { UserService } from '../services/user.service';
import { MemberService } from '../services/member.service';
import { FormsModule } from '../../../node_modules/@angular/forms';

@NgModule({
  imports: [
    CommonModule,SharedModule,FormsModule,RouterModule.forChild([
      {path:'register',component:RegisterComponent},
      {path:'login',component:LoginComponent}
    ])
  ],
  declarations: [RegisterComponent, LoginComponent],
  providers:[UserService,MemberService]
})
export class UserModule { }
