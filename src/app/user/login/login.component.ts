import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userName: string;
  password: string;
  constructor(private service: UserService, private router: Router) { }

  ngOnInit() {
  }
  logIn() {
    this.service.login(this.userName, this.password).subscribe(
      data => {
        localStorage.setItem('currentuser', JSON.stringify({ userName: data.userName, token: data.access_token }))
        this.router.navigate(['home']);
      },
      error => {
        alert(error.error.error_description)
      }
    );
  }
}
