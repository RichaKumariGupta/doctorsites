import { Component, OnInit } from '@angular/core';
import { Register } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 termsAndcondition:boolean; 
  user: Register = new Register();
  constructor(private service: UserService) { }

  ngOnInit() {
  }
  registered() {
    this.termsAndcondition=true;
    this.service.register(this.user).subscribe(
      data => {
        alert("Welcome! you are registered");
      },
      error => {
        alert(error);
      }
    );
  }
}
