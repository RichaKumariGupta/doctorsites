import { Component } from '@angular/core';
import { UserService } from './services/user.service';
import { Router } from '../../node_modules/@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(public service:UserService,private router:Router){}
    logOut(){
        this.service.logout().subscribe(
          data=>{
            localStorage.removeItem('currentuser');
            this.router.navigate(['/home']);
          },
          error =>{
            alert("LogOut Failed");
          }
        );
    }
}
