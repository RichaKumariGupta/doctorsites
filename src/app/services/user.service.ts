import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '../../../node_modules/@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { Register, login } from '../models/user';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  webApiUrl = environment.webApiUrl;
  constructor(private client: HttpClient) { }
  register(register: Register): Observable<Register> {
    return this.client.post<Register>(this.webApiUrl + "Api/Account/Register", register);
  }

  login(username: string, password: string): Observable<login> {
    let params = {
      grant_type: 'password',
      username: username,
      password: password
    }
    let headers = new HttpHeaders
      (
        { 'Content-Type': 'application/x-www-form-urlencoded' }
      );
    let options = { headers: headers }
    let body = this.encodeParams(params);
    return this.client.post<login>(this.webApiUrl + "token", body, options);
  }
  private encodeParams(params: any): string {
    let body: string = "";
    for (let key in params) {
      if (body.anchor.length) {
        body += "&";
      }
      body += key + "=";
      body += encodeURIComponent(params[key]);
    }
    return body;
  }
  getHeadersOption() {
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentuser')).token
    });
    let options = { headers: headers }
    return options;
  }
  isUserLoggedIn(): boolean {
    return localStorage.getItem('currentuser') != null
  }
  logout(): Observable<Object> {
    return this.client.post(this.webApiUrl + "Api/Account/LogOut", JSON.stringify({}), this.getHeadersOption())
  }

}
