import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { Gender, Clinic, Specialist, Profile, Practice, Experience } from '../models/CGSPP';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // webApiUrl="http://localhost:51308/Api/";
  webApiUrl=environment.webApiUrl+"Api/";

  constructor(private client: HttpClient) { }

  Gender(): Observable<Gender[]> {
    return this.client.get<Gender[]>(this.webApiUrl+"Gender")
  }
  Clinic(): Observable<Clinic[]> {
    return this.client.get<Clinic[]>(this.webApiUrl+"Clinic")
  }
  Specialist(): Observable<Specialist[]> {
    return this.client.get<Specialist[]>(this.webApiUrl+"Specialist")
  }
  Profile(): Observable<Profile[]> {
    return this.client.get<Profile[]>(this.webApiUrl+"Profile")
  }
  Practice(): Observable<Practice[]> {
    return this.client.get<Practice[]>(this.webApiUrl+"Practice")
  }
  Experience(): Observable<Experience[]> {
    return this.client.get<Experience[]>(this.webApiUrl+"Experience")
  }
}
