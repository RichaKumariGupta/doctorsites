import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { Member, SearchModel, MemberVM, MemberDetail } from '../models/member';
import { Observable } from '../../../node_modules/rxjs';
import { UserService } from './user.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
// webApiUrl="http://localhost:51308/Api/Member"
webApiUrl=environment.webApiUrl+"Api/Member";
  constructor(private client: HttpClient, private service: UserService) { }
  getProfile(): Observable<Member> {
    return this.client.get<Member>(this.webApiUrl, this.service.getHeadersOption());
  }
  saveProfile(id: number, member: Member): Observable<Member> {
    return this.client.put<Member>(this.webApiUrl + id, member, this.service.getHeadersOption())
  }
  searchMembers(model: SearchModel): Observable<MemberVM[]> {
    return this.client.post<MemberVM[]>(this.webApiUrl, model,this.service.getHeadersOption());
  }
  memberDetail(id:number):Observable<MemberDetail>{
    return this.client.get<MemberDetail>(this.webApiUrl+'/'+id,this.service.getHeadersOption());
  }
  deleteMember(id:number){
    return this.client.delete(this.webApiUrl+'/'+id,this.service.getHeadersOption());
  }
}
