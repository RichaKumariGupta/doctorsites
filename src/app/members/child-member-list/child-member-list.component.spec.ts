import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildMemberListComponent } from './child-member-list.component';

describe('ChildMemberListComponent', () => {
  let component: ChildMemberListComponent;
  let fixture: ComponentFixture<ChildMemberListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildMemberListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildMemberListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
