import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MemberVM } from '../../models/member';

@Component({
  selector: 'app-child-member-list',
  templateUrl: './child-member-list.component.html',
  styleUrls: ['./child-member-list.component.css']
})
export class ChildMemberListComponent implements OnInit, OnChanges {
  @Input() member;
  newmember: MemberVM = new MemberVM();
  constructor() { }

  ngOnInit() {
  }
  ngOnChanges() {
    this.newmember = this.member;
  }
}
