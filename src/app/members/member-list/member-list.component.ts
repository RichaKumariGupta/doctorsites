import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Gender, Practice, Specialist, Profile, Clinic } from '../../models/CGSPP';
import { MemberVM, SearchModel } from '../../models/member';
import { MemberService } from '../../services/member.service';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css']
})
export class MemberListComponent implements OnInit {
  genders: Gender[] = [];
  clinics: Clinic[] = [];
  profiles: Profile[] = [];
  practices: Practice[] = [];
  specialists: Specialist[] = [];
  experiences = [];
  searchModel:SearchModel= new SearchModel();
  members:MemberVM[]=[];
  searchTerm:string;
  constructor(private service:DataService,private memberService:MemberService) { 
    this.service.Gender().subscribe(
      data => {
        this.genders = data;
      }
    );
    this.service.Clinic().subscribe(
      data => {
        this.clinics = data;
      }
    );
    this.service.Practice().subscribe(
      data => {
        this.practices = data;
      }
    );
    this.service.Profile().subscribe(
      data => {
        this.profiles = data;
      }
    );
    this.service.Specialist().subscribe(
      data => {
        this.specialists = data;
      }
    );

    for (let i: number = 1; i <= 90; i++) {
      this.experiences.push(i);
    }
  }

  ngOnInit() {
  }
searchProfile(){
  this.memberService.searchMembers(this.searchModel).subscribe(
data=>{
  this.members= data;
}
  )
}
}
