import { PipeTransform, Pipe } from "../../../node_modules/@angular/core";
import { MemberVM } from "../models/member";
@Pipe({
name:'doctorFilter'
})

export class DoctorFilterPipe implements  PipeTransform{
    transform(doctors: MemberVM[], searchterm: string):MemberVM[] {
        if(!doctors || !searchterm){
            return doctors;
        }
        return doctors.filter(doctors=>doctors.name.toLowerCase().indexOf(searchterm.toLowerCase())!==-1);
    }
}