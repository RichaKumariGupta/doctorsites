import { Component, OnInit } from '@angular/core';
import { Gender, Clinic, Profile, Practice, Specialist, Experience } from '../../models/CGSPP';
import { DataService } from '../../services/data.service';
import { MemberService } from '../../services/member.service';
import { Member } from '../../models/member';
import { UserService } from '../../services/user.service';
import { Router } from '../../../../node_modules/@angular/router';
import { SelectItem } from '../../../../node_modules/primeng/components/common/selectitem';

@Component({
  selector: 'app-member-profile',
  templateUrl: './member-profile.component.html',
  styleUrls: ['./member-profile.component.css']
})
export class MemberProfileComponent implements OnInit {
  ages=[];
  genders: Gender[] = [];
  clinics: Clinic[] = [];
  profiles: Profile[] = [];
  practices: Practice[] = [];
  specialists: Specialist[] = [];
  experiences = [];
  member: Member = new Member();
  constructor(private service: DataService, private memberService: MemberService,private s: UserService,private router:Router) {
    this.memberService.getProfile().subscribe(
      data => {
        this.member = data;
        this.member.id;
      }
    );
    this.service.Gender().subscribe(
      data => {
        this.genders = data;
      }
    );
    this.service.Clinic().subscribe(
      data => {
        this.clinics = data;
        this.member.clinicId=-1;
      }
    );
    this.service.Practice().subscribe(
      data => {
        this.practices = data;
      }
    );
    this.service.Profile().subscribe(
      data => {
        this.profiles = data;
      }
    );
    this.service.Specialist().subscribe(
      data => {
        this.specialists = data;
        
      }
    );

    for (let i: number = 1; i <= 90; i++) {
      this.ages.push(i);
    }
    for (let i: number = 1; i <= 90; i++) {
      this.experiences.push(i);
    }
  }

  ngOnInit() {
  }
  saveProfile() {
    if(this.member.age== null || this.member.genderId== null || this.member.clinicId==null ||this.member.practiceId==null,this.member.profileId==null ){
      alert("Enter the empty text");
      return;
    }
    this.memberService.saveProfile(this.member.id, this.member).subscribe(
      
      data => {
        alert("Data Saved Successfully");
      },
      error => {
        alert("error");
      }
    );
  }
  deleteMember(id:number){
    if(confirm("Do you want to do delete")){
      this.memberService.deleteMember(id).subscribe(
        data=>{
          if(data== true){
            alert("Data deleted successfully");
            this.s.logout().subscribe(
             data=> {
                localStorage.removeItem('currentuser');
                this.router.navigate(['/home']);
              },
            )
          }
          else{
            alert("error");
          }
        }
      );
    } 
  }
}
