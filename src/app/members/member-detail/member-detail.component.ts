import { Component, OnInit } from '@angular/core';
import { MemberDetail, Member } from '../../models/member';
import { MemberService } from '../../services/member.service';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})
export class MemberDetailComponent implements OnInit {
  member:MemberDetail= new MemberDetail();
  memberId:number;
  constructor(private service:MemberService,private ar:ActivatedRoute) { 
    this.ar.paramMap.subscribe(param=>{
      this.memberId= parseInt(param.get("detail"));
      this.service.memberDetail(this.memberId).subscribe(
        data=>{
          this.member=data;
        }
      );
    }
  );
  }

  ngOnInit() {
  }

}
