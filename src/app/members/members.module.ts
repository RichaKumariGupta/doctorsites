import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MemberProfileComponent } from './member-profile/member-profile.component';
import { MemberListComponent } from './member-list/member-list.component';
import { RouterModule } from '../../../node_modules/@angular/router';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '../../../node_modules/@angular/forms';
import { ChildMemberListComponent } from './child-member-list/child-member-list.component';
import { MemberDetailComponent } from './member-detail/member-detail.component';
import { DoctorFilterPipe } from './doctor_filter.pipe';

@NgModule({
  imports: [
    CommonModule, SharedModule, FormsModule, RouterModule.forChild([
      { path: 'myProfile', component: MemberProfileComponent },
      { path: 'members', component: MemberListComponent },
      {path:'viewProfile/:detail',component:MemberDetailComponent}
    ])
  ],
  declarations: [MemberProfileComponent, MemberListComponent, DoctorFilterPipe,ChildMemberListComponent, MemberDetailComponent]
})
export class MembersModule { }
