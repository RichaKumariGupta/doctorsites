export class Member{
    id:number;
    name:string;
    email:string;
    mobNo:string;
    specialistId:number;
    genderId:number;
    practiceId:number;
    profileId:number;
    clinicId:number;
    experience:string;
    comment:string;
    date:Date;
    age:number;
}
export class MemberVM{
    id:number;
    name:string;
    specialist:string;
    clinic:string;
    mobNo:string;
    profile:string;
    date:Date
}
export class SearchModel{
    specialistIds:number[]=[];
    clinicIds:number[]=[];
    practiceIds:number[]=[];
    genderId:number;
    experience:string;
}
export class MemberDetail{
    name:string;
    email:string;
    mobNo:string;
    specialist:string;
    gender:string;
    practice:string;
    profile:string;
    clinic:string;
    experience:string;
    comment:string;
    date:Date;
    age:number;
}