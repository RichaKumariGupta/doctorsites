export class Gender{
    id:number;
    name:string;
}
export class Clinic{
    id:number;
    name:string;
}
export class Specialist{
    id:number;
    name:string;
}
export class Profile{
    id:number;
    name:string;
}
export class Practice{
    id:number;
    name:string;
}
export class Experience{
    id:number;
    name:string;
}